<?php

namespace mf\Controller;


abstract class Action{
    protected $view;

    function __construct(){
        $this->view = new \stdClass();
        //Class nativa Vazia que pode ser preenchia pela aplicação
    }
    protected function render($view, $layout){
        $this->view->page = $view;

        //Verifica se arquivo existe
        if(file_exists('../app/Views/'.$layout.'.phtml')){
            require_once '../app/Views/'.$layout.'.phtml';
        
        }else{
            $this->contend();
        }
    }
    protected function contend(){
        $classAtual = get_class($this);//Recupera nome da class
        $classAtual = str_replace('app\\Controllers\\' , '', $classAtual);//Substitue no texto
        $classAtual = strtolower(str_replace('Controller' , '', $classAtual));
        //Substitue e transforma texto em caixa-baixa

        require_once '../app/Views/'.$classAtual.'/'. $this->view->page.'.phtml';
    }

}

?>