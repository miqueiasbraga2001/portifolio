

$('#email').click(function () {

    $(this).css('display', 'none')
    $('.home').css('display', 'block')

    $('#conteudo').fadeOut(function () {
        $('#conteudo').html('')
        $('#conteudo').load('../load/email.php')
        $('#conteudo').fadeIn()
    })


})
$('.home').click(function () {

    $(this).css('display', 'none')
    $('#email').css('display', 'block')

})
$('.home-link').click(function () {

    $('#index_email').fadeOut(function () {
        $('#conteudo').html('')
        $('#conteudo').load('../load/links-areas.php')
    })

})
$('.home-web').click(function () {

    $('#index_email').fadeOut(function () {
        $('#conteudo').html('')
        $('#conteudo').load('../load/web.php')
    })

})
$('.home-mec').click(function () {

    $('#index_email').fadeOut(function () {
        $('#conteudo').html('')
        $('#conteudo').load('../load/mec.php')
    })

})
$('#form_email').submit(function () {
    $('.carregando').css('display', 'block')

    let dados = jQuery(this).serialize();

    $.ajax({
        url: '/enviar_email',
        cache: false,
        data: dados,
        type: 'POST',

        success: function (msg) {
            $('.carregando').fadeOut(500)
            $('.ok').css('display', 'block');
          
            const timer = setTimeout(function () {
                $('.ok').fadeOut(500)
            }, 1000)

            const timer2 = setTimeout(function () {
                $('.home').trigger('click');
            }, 1500)
        }
    })
    return false;
})

function _link(url) {
    window.open(url);
}