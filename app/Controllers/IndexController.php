<?php

namespace app\Controllers;
//Recurssos do Miniframework
use mf\Model\Container;
use mf\Controller\Action;

//Models
//use app\Models\Mensagem;
//use app\Models\Info;


class IndexController extends Action{
   
    function index(){

        $this->render('index', 'layout1');
    }
    function web(){
        
        $this->render('desenvolvimento-web', 'layout1');
    }
    function mec(){

        $this->render('engenharia-mecatronica', 'layout1');
    }
    function enviar_email(){

        $mensagem = Container::getModel('Mensagem');
        
        $mensagem->__set('email', $_POST['email']);
        $mensagem->__set('assunto', $_POST['assunto']);
        $mensagem->__set('mensagem', $_POST['mensagem']);

        print_r($mensagem);
        $mensagem->enviar();
        //$this->render('email', 'layout1');
    }

}

?>