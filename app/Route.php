<?php

namespace app;

use mf\Init\Bootstrap;

class Route extends Bootstrap{

    protected function initRouts(){
        $routes['home'] = array(
            'route' => '/',
            'controller' => 'indexController',
            'action' => 'index'
        );
        $routes['mec'] = array(
            'route' => '/engenharia-mecatronica',
            'controller' => 'indexController',
            'action' => 'mec'
        );
        $routes['web'] = array(
            'route' => '/desenvolvimento-web',
            'controller' => 'indexController',
            'action' => 'web'
        );

        ///////////////// SEÇOES ///////////////
        $routes['enviar_email'] = array(
            'route' => '/enviar_email',
            'controller' => 'indexController',
            'action' => 'enviar_email'
        );
        $this->setRoutes($routes);

    }
}
?>